clc;clear;

i=7
j=7

fsky=0.436;
gamrms=0.26;
ng=26;

filename=fullfile('Spectrums','fid','shear_cl','ell.txt');
l=dlmread(filename,' ',1,0);
slt=find(20<=l & l<=5000);
l=l(slt);

t=['bin_',int2str(j),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','shear_cl',t);
GGij=dlmread(filename,' ',1,0);
GGij=GGij(slt);

t=['bin_',int2str(i),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','shear_cl',t);
GGii=dlmread(filename,' ',1,0);
GGii=GGii(slt);

t=['bin_',int2str(j),'_',int2str(j),'.txt'];
filename=fullfile('Spectrums','fid','shear_cl',t);
GGjj=dlmread(filename,' ',1,0);
GGjj=GGii(slt);

t=['bin_',int2str(i),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','galaxy_cl',t);
P=dlmread(filename,' ',1,0);
gg=P(slt);

filename=fullfile('SCterms',int2str((i+2)*100+i+2),'C_Gg.dat');
P=load(filename);
P=flipud(P(:,5));
Q=P(slt);

load('nbar_i.txt');
GGN=gamrms^2./(ng*(60*180/pi)^2 *nbar_i *4*pi*fsky);
ggN=1./(ng*(60*180/pi)^2 *nbar_i *4*pi*fsky);

t=['bin_',int2str(i),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','shear_cl_ii',t);
P=dlmread(filename,' ',1,0);
II=P(slt);

t=['bin_',int2str(j),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','shear_cl_gi',t);
P=dlmread(filename,' ',1,0);
IG=P(slt);

filename=fullfile('SCterms',int2str((i+2)*100+j+2),'C_IG.dat');
P=load(filename);
P=flipud(P(:,3));
Wij=P(slt);

t=['bin_',int2str(i),'_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','galaxy_intrinsic_cl',t);
P=dlmread(filename,' ',1,0);
Ig=P(slt);

filename=fullfile('SCterms',int2str((i+2)*100+i+2),'C_Ig.dat');
P=load(filename);
P=flipud(P(:,4));
Delta_inv=P(slt);

t=['bin_',int2str(i),'.txt'];
filename=fullfile('Spectrums','fid','nz_sample',t);
nz_i=dlmread(filename,' ',1,0);
nz_i=nz_i(2:length(nz_i)-1);

filename=fullfile('Spectrums','fid','bias_field','b_g.txt');
b_g=dlmread(filename,' ',1,0);
b_g=b_g(1,:);

bi=b_g*nz_i*0.05; % 0.05 is step length in the n(z) histogram

e_ij=IG./Wij.*Delta_inv./Ig*bi-1;

D_Ig_ii=sqrt( 1/2./(2*l+1)./(0.2*l) .* ( gg.*GGii + (1+1./(3*(1-Q).^2)) .* (gg.*GGN(i)+ggN(i).*(GGii+II)) + ggN(i)*GGN(i)*(1+1./(1-Q).^2) ) );
D_b_b=1/2* sqrt(2./(2*l+1)./(0.2*l)/fsky) .*(1+ggN(i)./gg);
f1=D_Ig_ii.*Wij./Delta_inv/bi./GGij;
f2=D_b_b.*Wij./Delta_inv.*(-Ig)/bi./GGij;

e_min_ij=sqrt( ( GGij.^2+(GGii+GGN(i)).*(GGjj+GGN(j)) ) ./ (2*l*0.2.*l*fsky.*GGij.^2) );

figure;
plot(l,e_min_ij,'k');
set(gca,'xscale','log');
set(gca,'yscale','log');
lgd{1}='\Delta C^{GG}/C^{GG}';

hold on;
plot(l,f1,'b--');
lgd{2}='C^{IG(SC)}\Delta C^{Ig}/C^{Ig} / C^{GG}';
plot(l,f2,'r--');
lgd{3}='C^{IG(SC)}\Delta b_i/b_i / C^{GG}';
hold off;
xlabel('$\ell$','Interpreter','LaTex');
ylabel('propagated measurement error')

legend(lgd);

figure;

loglog(l,-D_Ig_ii./Ig,'b');
lgd2{1}='|\Delta C^{Ig} / C^{Ig}|';
hold on;
loglog(l,D_b_b,'r');
lgd2{2}='\Delta b_i/b_i';
hold off;
ylabel('observed measurement error');
xlabel('$\ell$','Interpreter','LaTex');

legend(lgd2);