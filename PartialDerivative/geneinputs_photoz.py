import ConfigParser
config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')

#partial=0.15
#dwa=0.3

sigma_z=float(config.get('photo-z parameters','sigma_z'))
Delta_z=float(config.get('photo-z parameters','Delta_z'))

dsigma_z=float(config.get('partial derivatives','dsigma_z'))
dDelta_z=float(config.get('partial derivatives','dDelta_z'))

config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')
t=sigma_z + dsigma_z
config.set('photo-z parameters','sigma_z',t)
with open(r'photoz_para_2p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')
t=sigma_z - dsigma_z
config.set('photo-z parameters','sigma_z',t)
with open(r'photoz_para_2m.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')
t=Delta_z + dDelta_z
config.set('photo-z parameters','Delta_z',t)
with open(r'photoz_para_1p.ini', 'wb') as configfile:
    config.write(configfile)

config= ConfigParser.RawConfigParser()
config.read(r'photoz_para.ini')
t=Delta_z - dDelta_z
config.set('photo-z parameters','Delta_z',t)
with open(r'photoz_para_1m.ini', 'wb') as configfile:
    config.write(configfile)
	
print '~~~~ Complete: Generating input files for partial derivatives of photo-z parameters ~~~~'