#!/bin/bash

# SRUN OPTIONS
# NOTE: #SBATCH is a command, not a comment
# ----------------------------------------------------------------------
# NECESSARY PARAMETERS

# The number of tasks
#SBATCH -n 1

# ----------------------------------------------------------------------
# OPTIONAL PARAMETERS

# The number of cores per task (used for multi-threaded application, default=1,
# prefer a power of 6, no more than the max number of cores per node)
# Change this when using openMP in camb for CosmoMC.  Only advantageous when calculating
# CMB and MPK
#SBATCH -c 6
##SBATCH -c 8
# The number of tasks per node (n * c)
# Can uncomment this to force x number of tasks per node.
##SBATCH --tasks-per-node=1

# The number of nodes
# SLURM issues a warning if you specify more nodes than tasks (N > n).
# It's better to let slurm calculate it for you.
# IMPORTANT: MPI between nodes will slow down the program because of the
# heavy I/O. It's recommended to more cores and less nodes for our jobs.
# Can change below to run MPI jobs across multiple nodes
#SBATCH -N 1
##SBATCH -N 4

# Use --exclusive to get the whole nodes exclusively for this job
##SBATCH --exclusive

# Request a sepcific list of hosts (two different formats)
#SBATCH -w f7
##SBATCH -w ./hostfile
## Request a specific list of hosts NOT be included
#SBATCH --exclude=cosmo,f1
# Setting the name of the error-file to 'job.myjobid.err' and
# the output-file to 'job.myjobid.out'
# The file paths below are relative to the directory from which you submitted
# Change to your preferences.
#SBATCH --error=%J.err --output=%J.out

#Print  detailed  event  logging to error file
#SBATCH -v

#Give your job a name, so you can more easily identify which job is which
#SBATCH -J TomoTest
# ------------------------------
# VERY OPTIONAL PARAMETERS

# Account name (project ID) to run under
##SBATCH -A <account>

# The maximum allowed run time (D-HH:MM:)
##SBATCH --time=15-00:00:00

# If this job needs 4GB of memory per mpi-task (=mpi ranks, =cores)
##SBATCH --mem-per-cpu=4000

# ----------------------------------------------------------------------
# MPI SET-UP FOR SLURM

# Different types of MPI may result in unique initiation procedures.
# IMPORTANT for mpich2: user assumes the system administrator already link
# your program with SLURM's implementation of the PMI library
# If SLURM is not configured with MpiDefault=pmi2
# then the srun command MUST BE invoked with the option --mpi=pmi2.
# Reference: http://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_How_do_I_use_MPICH_with_slurm.3F
# Reference: http://slurm.schedmd.com/mpi_guide.html#mpich2
# ---------------------------------------------------------------------



# Run the MPI application

#uncomment the line below if you want to run with openMP as advised above.
#change number of threads to match -c line above.
#export OMP_NUM_THREADS=6

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"

# Changing to director where my application is since I submit from scripts

mkdir Spectrums
cd Spectrums
mkdir fid

mkdir 1p
mkdir 1m
mkdir 2p
mkdir 2m
mkdir 3p
mkdir 3m
mkdir 4p
mkdir 4m

mkdir 7p
mkdir 7m
mkdir 8p
mkdir 8m

mkdir 9p
mkdir 9m
mkdir 10p
mkdir 10m

mkdir 11p
mkdir 11m
mkdir 12p
mkdir 12m
cd ../PartialDerivative/
python geneinputs.py
python geneinputs_photoz.py
cd ../..
source setup-my-cosmosis

## ----------------- cosmological parameters -----------------

cp SelfCalibration/PartialDerivative/photoz_para.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..

cp SelfCalibration/MyAdjustedModules/my_clerkin_interface.py cosmosis-standard-library/bias/clerkin/my_clerkin_interface.py
cp SelfCalibration/MyAdjustedModules/my_ia_z_powerlaw.py cosmosis-standard-library/intrinsic_alignments/z_powerlaw/my_ia_z_powerlaw.py

cp SelfCalibration/PartialDerivative/valuesSC6_fiducial.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/SpectraSC.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/fid
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/fid
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/fid
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/fid
cp -a SelfCalibration/Calculation/scterms SelfCalibration/Spectrums/fid

cp SelfCalibration/PartialDerivative/valuesSC6_1p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/1p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/1p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/1p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/1p

cp SelfCalibration/PartialDerivative/valuesSC6_1m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/1m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/1m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/1m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/1m

cp SelfCalibration/PartialDerivative/valuesSC6_2p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/2p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/2p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/2p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/2p

cp SelfCalibration/PartialDerivative/valuesSC6_2m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/2m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/2m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/2m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/2m

cp SelfCalibration/PartialDerivative/valuesSC6_3p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/3p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/3p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/3p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/3p

cp SelfCalibration/PartialDerivative/valuesSC6_3m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/3m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/3m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/3m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/3m

cp SelfCalibration/PartialDerivative/valuesSC6_4p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/4p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/4p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/4p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/4p

cp SelfCalibration/PartialDerivative/valuesSC6_4m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/4m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/4m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/4m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/4m

cp SelfCalibration/PartialDerivative/valuesSC6_7p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/7p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/7p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/7p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/7p

cp SelfCalibration/PartialDerivative/valuesSC6_7m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/7m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/7m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/7m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/7m

cp SelfCalibration/PartialDerivative/valuesSC6_8p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/8p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/8p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/8p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/8p

cp SelfCalibration/PartialDerivative/valuesSC6_8m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/8m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/8m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/8m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/8m

## ----------------- photo-z parameters -----------------

cp SelfCalibration/PartialDerivative/valuesSC6_fiducial.ini SelfCalibration/valuesSC6.ini

cp SelfCalibration/PartialDerivative/photoz_para_1p.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/9p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/9p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/9p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/9p

cp SelfCalibration/PartialDerivative/photoz_para_1m.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/9m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/9m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/9m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/9m

cp SelfCalibration/PartialDerivative/photoz_para_2p.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/10p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/10p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/10p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/10p

cp SelfCalibration/PartialDerivative/photoz_para_2m.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/10m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/10m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/10m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/10m

## ----------------- IA parameters -----------------

cp SelfCalibration/PartialDerivative/photoz_para.ini SelfCalibration/photoz_para.ini
cd SelfCalibration
python nz_tomo.py
cd ..

cp SelfCalibration/PartialDerivative/valuesSC6_11p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/11p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/11p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/11p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/11p

cp SelfCalibration/PartialDerivative/valuesSC6_11m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/11m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/11m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/11m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/11m

cp SelfCalibration/PartialDerivative/valuesSC6_12p.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/12p
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/12p
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/12p
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/12p

cp SelfCalibration/PartialDerivative/valuesSC6_12m.ini SelfCalibration/valuesSC6.ini
cosmosis SelfCalibration/Spectra.ini
cp -a SelfCalibration/Calculation/shear_cl* SelfCalibration/Spectrums/12m
cp -a SelfCalibration/Calculation/galaxy*cl SelfCalibration/Spectrums/12m
cp -a SelfCalibration/Calculation/bias_field SelfCalibration/Spectrums/12m
cp -a SelfCalibration/Calculation/nz_sample SelfCalibration/Spectrums/12m

cd SelfCalibration/SCtermsCode

mkdir ./SCterms/

# bash runQ.sh

cd ..

echo "Program finished with exit code $? at: `date`"

# ---------------------------------------------------
# Reference: http://www.hpc2n.umu.se/batchsystem/examples_scripts,
#            http://www.hpc2n.umu.se/slurm-submit-file-design
#            https://computing.llnl.gov/tutorials/linux_clusters/man/srun.txt
# ---------------------------------------------------
