% new SC method, need to run FisherMatrixSIS_tomo.m to read data

Aij=zeros(Nbin,Nbin,length(slt)); % Aij is A^Ig_ij
GI=zeros(Nbin,Nbin,length(slt));

for i=1:length(slt)
  tmp=Ig./gg;
  Aij(:,:,i)=diag(diag(tmp(:,:,i)));
  GI(:,:,i)=IG(:,:,i)';
end;

for ll=1:length(slt)
  for i=1:Nbin
    for j=i+1:Nbin
      Aij(i,j,ll)=-sqrt(Aij(i,i,ll)*Aij(j,j,ll));
      % Aij(i,j,ll)=mean([Aij(i,i,ll) Aij(j,j,ll)]);
      Aij(j,i,ll)=Aij(i,j,ll);
    end;
  end;
end;

eff_II=Aij.^2.*gg./II;
eff_GI=Aij.*Gg./GI;

%% effeciency of II
figure;
eff1=reshape(eff_II(2,3,:),[1 27]);
eff2=reshape(eff_II(5,6,:),[1 27]);
eff3=reshape(eff_II(1,7,:),[1 27]);
plot(l,eff1); lgdII{1}=['( i, j ) = ( 2, 3 )'];
hold on;
plot(l,eff2); lgdII{2}=['( i, j ) = ( 5, 6 )'];
plot(l,eff3); lgdII{3}=['( i, j ) = ( 1, 7 )'];
hold off;
set(gca,'xscale','log');
xlabel('$\ell$','Interpreter','LaTex','Fontsize',15);
ylabel(['efficiency of C^{II}_{ij}'],'Fontsize',15);
legend(lgdII);

%% effeciency of GI
figure;
eff1=reshape(eff_GI(4,4,:),[1 27]);
eff2=reshape(eff_GI(3,5,:),[1 27]);
eff3=reshape(eff_GI(5,3,:),[1 27]);
eff4=reshape(eff_GI(6,2,:),[1 27]);
plot(l,eff1); lgdGI{1}=['( i, j ) = ( 4, 4 )'];
hold on;
plot(l,eff2); lgdGI{2}=['( i, j ) = ( 3, 5 )'];
plot(l,eff3); lgdGI{3}=['( i, j ) = ( 5, 3 )'];
plot(l,eff4); lgdGI{4}=['( i, j ) = ( 6, 2 )'];
hold off;
set(gca,'xscale','log');
xlabel('$\ell$','Interpreter','LaTex','Fontsize',15);
ylabel(['efficiency of C^{GI}_{ij}'],'Fontsize',15);
legend(lgdGI);

%% error analysis II

D_gg = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  D_gg(:,:,ll) = sqrt(2/(2*l(ll)+1)/0.2/l(ll)) * ( gg(:,:,ll) + diag(ggN) );
end;

DC_Ig_ii = zeros(Nbin,Nl);
for i=1:Nbin
  DC_Ig_ii(i,:) = sqrt( (1/2./(2*l+1)./(0.2*l))' .* ( reshape(gg(i,i,:).*GG(i,i,:),[1 Nl]) + (1+1./(3*(1-Q(i,:)).^2)) .* (reshape(gg(i,i,:),[1 Nl]).*GGN(i)+ggN(i).*reshape(GG(i,i,:)+II(i,i,:),[1 Nl])) + ggN(i)*GGN(i)*(1+1./(1-Q(i)).^2) ) );
end;

DII_II2 = zeros(Nbin,Nbin,Nl); % ( DeltaII_ij / II_ij )^2
DII_GG = zeros(Nbin,Nbin,Nl); % DeltaII_ij / GG_ij
DGG_GG = zeros(Nbin,Nbin,Nl); % DeltaGG_ij / GG_ij
for ll=1:Nl
  for i=1:Nbin
    for j=1:Nbin
      DII_II2(i,j,ll) = (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 + (D_gg(i,j,ll)/gg(i,j,ll))^2 ;
      DII_GG(i,j,ll) = sqrt(DII_II2(i,j,ll)) * II(i,j,ll) / GG(i,j,ll);
      DGG_GG(i,j,ll) = sqrt( ( (GG(i,j,ll)+(i==j)*GGN(i)).^2+(GG(i,i,ll)+GGN(i)).*(GG(j,j,ll)+GGN(j)) ) ./ (2*l(ll)*0.2*l(ll)*GG(i,j,ll).^2) );
    end;
  end;
end;

['SC II introduced error max = ' num2str(max(max(max( DII_GG./DGG_GG ))))]
['SC II introduced error mean = ' num2str(mean(mean(mean( DII_GG./DGG_GG ))))]

%% error analysis GI

DC_GgIg_ij = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  DC_GgIg_ij(:,:,ll) = sqrt(2/(2*l(ll)+1)/0.2/l(ll)) * abs( Gg(:,:,ll) + Ig(:,:,ll) );
end;

DC_Gg_ij2 = zeros(Nbin,Nbin,Nl);
DGI_GI = zeros(Nbin,Nbin,Nl);
DGI_GG = zeros(Nbin,Nbin,Nl);
for ll=1:Nl
  for i=1:Nbin
    for j=1:Nbin
      DC_Gg_ij2(i,j,ll) = DC_GgIg_ij(i,j,ll)^2 + 1/2 * ( (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 ) * (Aij(i,j,ll)*gg(i,j,ll))^2;
      DGI_GI(i,j,ll) = 1/2 * ( (DC_Ig_ii(i,ll)/Ig(i,i,ll))^2 + (D_gg(i,i,ll)/gg(i,i,ll))^2 + (DC_Ig_ii(j,ll)/Ig(j,j,ll))^2 + (D_gg(j,j,ll)/gg(j,j,ll))^2 ) + DC_Gg_ij2(i,j,ll)/Gg(i,j,ll)^2;
      DGI_GG(i,j,ll) = DGI_GI(i,j,ll) * IG(j,i,ll) / GG(i,j,ll);
    end;
  end;
end;

['SC GI introduced error max = ' num2str(min(min(min( DGI_GG./DGG_GG ))))]
['SC GI introduced error mean = ' num2str(mean(mean(mean( DGI_GG./DGG_GG ))))]

%% fast forecast
